# Crayon - Zsír-KRéTA

This program generates and sends email reports from the current student news
and infos found in the Hungarian elementary school administration system,
called [Kréta][0].

It can generate report for multiple students (e.g. for siblings), and send
the reports to multiple email addresses.

## Usage

This program intended to be called periodically (e.g. by a *cron job*,
or *Windows Task*).

You'll need the students' credentials and one for an smtp server. You can
put these into the `appsettings.json` file or pass as command line arguments.
Or even mix the two methods. Command line arguments taking precedence.

### Sample appsettings.json file

    {
        "studentProfiles": [
            {
                "name": "{student's name in reports}",
                "serverAddress": "https://{school's id}.e-kreta.hu",
                "userId": "{studnet's user id}",
                "password": "{student's password}",
                "email": "{student's email}"
            }
        ],
        "parentEmails": [ "{parent's email}" ],
        "errorReportEmail": "{email to send error reports to}",
        "notifier": {
            "serverAddress": "{SMTP server name}",
            "port": {SMTP port},
            "username": "{account user name}",
            "password": "{account password}",
            "fromAddress": "{email the reports will be sent in behalf of}"
        }
    }


[0]: https://tudasbazis.ekreta.hu/