using System;

namespace Crayon.Cli
{
    public class Notification
    {
        public Notification(
            string title,
            string content,
            string author,
            DateTimeOffset date)
        {
            Title = title;
            Content = content;
            Author = author;
            Date = date;
        }

        public string Title { get; }

        public string Content { get; }

        public string Author { get; }

        public DateTimeOffset Date { get; }
    }
}