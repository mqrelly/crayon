using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;
using System.Threading.Tasks;
using System.Xml;

namespace Crayon.Cli
{
    public class CrayonClient
    {
        public CrayonClient(string serverAddress)
        {
            this.serverAddress = serverAddress;
            client = new HttpClient(
                new HttpClientHandler
                {
                    AllowAutoRedirect = true,
                    UseCookies = true,
                    CookieContainer = cookies
                });
        }

        private readonly string serverAddress;

        private readonly CookieContainer cookies = new CookieContainer();

        private readonly HttpClient client;

        public async Task<bool> Login(string userId, string password)
        {
            var resp = await client
                .PostAsync(
                    $"{serverAddress}/Adminisztracio/Login/LoginCheck",
                    JsonContent.Create(new
                    {
                        UserName = userId,
                        Password = password,
                        ReCaptchaIsEnabled = false
                    }))
                .ConfigureAwait(false);

            if ((int)resp.StatusCode < 200 || 400 <= (int)resp.StatusCode)
            {
                return false;
            }

            var respBody = await resp.Content.ReadAsStringAsync().ConfigureAwait(false);
            using var respJson = JsonDocument.Parse(respBody);
            if (!respJson.RootElement.GetProperty("Success").GetBoolean())
            {
                return false;
            }

            respBody = await client
                .GetStringAsync($"{serverAddress}/Intezmeny/Faliujsag")
                .ConfigureAwait(false);
            var reqVerificationToken = ExtractVerificationToken(respBody);
            if (reqVerificationToken is null)
            {
                return false;
            }
            cookies.Add(new Cookie("__RequestVerificationToken", reqVerificationToken, "/", "e-kreta.hu"));

            return true;
        }

        public Task<IReadOnlyList<Test>> GetTests()
        {
            return GetData(
                "/api/TanuloBejelentettSzamonkeresekApi/GetBejelentettSzamonkeresekGrid?page=1&pageSize=100&data={\"RegiSzamonkeresekElrejtese\":true}",
                json => new Test(
                    json.GetProperty("TargyNev").GetString(),
                    json.GetProperty("SzamonkeresDatuma").GetDateTimeOffset().Date,
                    json.GetProperty("SzamonkeresMegnevezes").GetString()));
        }

        public Task<IReadOnlyList<SubjectNote>> GetSubjectNotes()
        {
            return GetData(
                "/api/InformaciokTanorakApi/GetTanitasiOraGrid?sort=Datum-desc~OraSorsz-desc&page=1&pageSize=100&data={}",
                json => new SubjectNote(
                    json.GetProperty("TargyNev").GetString(),
                    json.GetProperty("Tema").GetString(),
                    json.GetProperty("Datum").GetDateTimeOffset().Date));
        }

        public Task<IReadOnlyList<Homework>> GetHomework()
        {
            return GetData(
                "/api/TanuloHaziFeladatApi/GetTanulotHaziFeladatGrid?sort=HaziFeladatHatarido-asc&page=1&pageSize=100&data={\"RegiHaziFeladatokElrejtese\":true}",
                json => new Homework(
                    json.GetProperty("TantargyNev").GetString(),
                    json.GetProperty("HaziFeladatHatarido").GetDateTimeOffset().Date,
                    json.GetProperty("HaziFeladaSzoveg").GetString()));
        }

        public Task<IReadOnlyList<Notification>> GetNotifications()
        {
            return GetData(
                "/api/FeljegyzesekApi/GetInformaciokFeljegyzesekGrid?sort=Datum-desc&page=1&pageSize=100&data={}",
                json => new Notification(
                    json.GetProperty("Cim").GetString(),
                    json.GetProperty("Tartalom").GetString(),
                    json.GetProperty("Tanar").GetString(),
                    json.GetProperty("Datum").GetDateTimeOffset().Date));
        }

        public async Task<IReadOnlyList<Grade>> GetGrades()
        {
            var embedderHtml = await client
                .GetStringAsync($"{serverAddress}/TanuloErtekeles/Osztalyzatok")
                .ConfigureAwait(false);
            var studentId = ExtractStudentId(embedderHtml);
            var jobId = ExtractJobId(embedderHtml);

            var gradesPerSubjects = await GetData(
                $"/api/TanuloErtekelesByTanuloApi/GetTanuloErtekelesByTanuloGridTanuloView?data={{\"tanuloId\":\"{studentId}\",\"oktatasiNevelesiFeladatId\":\"{jobId}\"}}",
                json => (json.GetProperty("TantargyNev").GetString(),
                         new string[]
                         {
                             json.GetProperty("Szeptember").GetString(),
                             json.GetProperty("Oktober").GetString(),
                             json.GetProperty("November").GetString(),
                             json.GetProperty("December").GetString(),
                             json.GetProperty("JanuarI").GetString(),
                             json.GetProperty("JanuarII").GetString(),
                             json.GetProperty("Februar").GetString(),
                             json.GetProperty("Marcius").GetString(),
                             json.GetProperty("Aprilis").GetString(),
                             json.GetProperty("Majus").GetString(),
                             json.GetProperty("Junius").GetString(),
                         }))
                .ConfigureAwait(false);

            return gradesPerSubjects
                .SelectMany(ExtractGradesForSubject)
                .ToList()
                .AsReadOnly();

            static string ExtractStudentId(string html)
            {
                var startIdx = html.IndexOf("id=\"TanuloId\"");
                if (startIdx == -1)
                {
                    throw new KeyNotFoundException("TanuloId hidden input field not found in the embedder html.");
                }
                startIdx = html.IndexOf("value=", startIdx + 13) + 7;
                var endIdx = html.IndexOf('\"', startIdx);
                return html.Substring(startIdx, endIdx - startIdx);
            }

            static string ExtractJobId(string html)
            {
                var startIdx = html.IndexOf("?OktatasiNevelesiFeladatId=") + 27;
                if (startIdx == -1)
                {
                    throw new KeyNotFoundException("OktatasiNevelesiFeladatId parameter not found in the embedder html.");
                }
                var endIdx = html.IndexOf('&', startIdx);
                return html.Substring(startIdx, endIdx - startIdx);
            }

            static IEnumerable<Grade> ExtractGradesForSubject((string, string[]) subjectData)
            {
                var (subject, htmlFragsPerMonth) = subjectData;

                return htmlFragsPerMonth
                    .SelectMany(htmlFragment => ExtractGradesForSubjectMonth(subject, htmlFragment));
            }

            static IEnumerable<Grade> ExtractGradesForSubjectMonth(string subject, string htmlFragment)
            {
                if (string.IsNullOrEmpty(htmlFragment))
                {
                    yield break;
                }

                using var reader = new System.IO.StringReader(htmlFragment.Replace("<br />", " "));
                using var htmlReader = XmlReader.Create(reader, new XmlReaderSettings
                {
                    IgnoreComments = true,
                    IgnoreProcessingInstructions = true,
                    IgnoreWhitespace = true,
                    ConformanceLevel = ConformanceLevel.Fragment
                });

                while (htmlReader.Read())
                {
                    if (htmlReader.NodeType != XmlNodeType.Element ||
                       !htmlReader.Name.Equals("span", StringComparison.InvariantCultureIgnoreCase))
                    {
                        continue;
                    }

                    yield return new Grade(
                        subject,
                        htmlReader.GetAttribute("data-ertekelestema"),
                        htmlReader.GetAttribute("data-ertekelonyomtatasinev"),
                        DateTimeOffset.Parse(htmlReader.GetAttribute("data-datum")).Date,
                        htmlReader.GetAttribute("data-tipusmod"),
                        htmlReader.GetAttribute("data-tanuloertekeles"));
                }
            }
        }

        private string? ExtractVerificationToken(string body)
        {
            var idx = body.IndexOf("__RequestVerificationToken");
            if (idx == -1)
            {
                return null;
            }

            idx = body.IndexOf("value=", idx + 27) + 7;
            var length = body.IndexOf('"', idx) - idx;
            return body.Substring(idx, length);
        }

        private async Task<IReadOnlyList<T>> GetData<T>(string request, Func<JsonElement, T> construct)
        {
            var respBody = await client
                  .GetStringAsync($"{serverAddress}{request}")
                  .ConfigureAwait(false);

            using var respJson = JsonDocument.Parse(respBody);
            var data = respJson.RootElement.GetProperty("Data");

            return data
                .EnumerateArray()
                .Select(construct)
                .ToList()
                .AsReadOnly();
        }
    }
}