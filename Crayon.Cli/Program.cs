﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace Crayon.Cli
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Zsír-KRéTA");

            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: false)
                .AddCommandLine(args)
                .Build();

            var parentEmails = config.GetSection("parentEmails").Get<string[]>();
            var profiles = config.GetSection("studentProfiles").Get<StudentProfile[]>();
            var notifierConfig = config.GetSection("notifier").Get<EmailNotifier.Config>();
            var notifier = new EmailNotifier(notifierConfig);
            var errorReportEmail = config.GetSection("errorReportEmail").Get<string>();

            foreach (var profile in profiles)
            {
                try
                {
                    Console.Write($"Processing {profile.Name}... ");

                    var (hasAnyNews, title, report) = await GenerateReportForProfile(profile).ConfigureAwait(false);
                    if (hasAnyNews)
                    {
                        var emails = (parentEmails ?? new string[0]).Append(profile.Email).ToArray();
                        notifier.SendReport("Zsír-KRéTA: " + title, report, emails);
                        Console.WriteLine("done");
                    }
                    else
                    {
                        Console.WriteLine("nothing to report");
                    }
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine(ex);

                    notifier.SendReport("Zsír-KRéTA  Hiba!", ex.ToString(), errorReportEmail);
                }
            }
        }

        private static async Task<(bool, string, string)> GenerateReportForProfile(StudentProfile profile)
        {
            var client = new CrayonClient(profile.ServerAddress);
            if (!await client.Login(profile.UserId, profile.Password).ConfigureAwait(false))
            {
                throw new ApplicationException("Login failed!");
            }

            var today = DateTimeOffset.Now.Date;
            var nextSchoolDay = today.DayOfWeek == DayOfWeek.Friday ? today.AddDays(3) : today.AddDays(1);
            var displayCulture = System.Globalization.CultureInfo.GetCultureInfo("hu-HU");

            var title = $"{profile.Name} - {today.ToString("MMMM d., dddd", displayCulture)}";

            var report = new StringBuilder();
            report.AppendLine(title);
            report.AppendLine(new string('=', title.Length));

            var anyNews = false;

            var notifications = (await client.GetNotifications().ConfigureAwait(false))
                .Where(n => n.Date == today)
                .ToArray();
            if (notifications.Length > 0)
            {
                report.AppendLine($"\nÉrtesítések\n-----------");
                foreach (var n in notifications)
                {
                    report.AppendLine($"{n.Title} ({n.Author}):\n{n.Content}\n");
                }
                anyNews = true;
            }

            var subjectNotes = (await client.GetSubjectNotes().ConfigureAwait(false))
                .Where(sn => sn.Date == today)
                .ToArray();
            if (subjectNotes.Length > 0)
            {
                report.AppendLine("\nTanórák\n-------");
                foreach (var sn in subjectNotes)
                {
                    report.AppendLine($"{sn.Subject}: {sn.Note}");
                }
                anyNews = true;
            }

            var homework = (await client.GetHomework().ConfigureAwait(false))
                .Where(hw => hw.Deadline <= nextSchoolDay)
                .ToArray();
            if (homework.Length > 0)
            {
                report.AppendLine("\nHázi feladat\n------------");
                foreach (var hw in homework)
                {
                    report.AppendLine($"{hw.Subject}: {hw.Content}");
                }
                anyNews = true;
            }

            var tests = await client.GetTests().ConfigureAwait(false);
            if (tests.Count > 0)
            {
                report.AppendLine("\nSzámonkérés\n-----------");
                foreach (var test in tests)
                {
                    report.AppendLine($"{test.Subject} ({test.Date:d}): {test.Content}");
                }
                anyNews = true;
            }

            var grades = (await client.GetGrades().ConfigureAwait(false))
                .Where(gr => gr.Date == today)
                .ToArray();
            if (grades.Length > 0)
            {
                report.AppendLine("\nJegyek\n------");
                foreach (var grade in grades)
                {
                    report.AppendLine($"{grade.Subject} ({grade.Date:d}): {grade.Value}  ({grade.Content})");
                }
                anyNews = true;
            }

            return (anyNews, title, report.ToString());
        }

        class StudentProfile
        {
            public string Name { get; set; }

            public string ServerAddress { get; set; }

            public string UserId { get; set; }

            public string Password { get; set; }

            public string Email { get; set; }
        }
    }
}
