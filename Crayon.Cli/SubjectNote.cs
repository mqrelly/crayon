using System;

namespace Crayon.Cli
{
    public class SubjectNote
    {
        public SubjectNote(
            string subject,
            string note,
            DateTimeOffset date)
        {
            Subject = subject;
            Note = note;
            Date = date;
        }

        public string Subject { get; }

        public string Note { get; }

        public DateTimeOffset Date { get; }
    }
}