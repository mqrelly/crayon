using System;

namespace Crayon.Cli
{
    public class Homework
    {
        public Homework(
            string subject,
            DateTimeOffset deadline,
            string content)
        {
            Subject = subject;
            Deadline = deadline;
            Content = content;
        }

        public string Subject { get; }

        public DateTimeOffset Deadline { get; }

        public string Content { get; }
    }
}