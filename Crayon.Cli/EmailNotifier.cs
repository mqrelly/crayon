using System.Net.Mail;

namespace Crayon.Cli
{
    public class EmailNotifier
    {
        public class Config
        {
            public string ServerAddress { get; set; }

            public string Username { get; set; }

            public string Password { get; set; }

            public string FromAddress { get; set; }

            public int Port { get; set; }
        }

        public EmailNotifier(Config config)
        {
            client = new SmtpClient(config.ServerAddress, config.Port)
            {
                EnableSsl = true,
                Credentials = new System.Net.NetworkCredential(config.Username, config.Password)
            };

            fromAddress = config.FromAddress;
        }

        private readonly SmtpClient client;
        private readonly string fromAddress;

        public void SendReport(string subject, string report, params string[] addresses)
        {
            client.Send(
                fromAddress,
                string.Join(',', addresses),
                subject,
                report);
        }
    }
}