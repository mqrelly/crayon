using System;

namespace Crayon.Cli
{
    public class Test
    {
        public Test(
            string subject,
            DateTimeOffset date,
            string content)
        {
            Subject = subject;
            Date = date;
            Content = content;
        }

        public string Subject { get; }

        public DateTimeOffset Date { get; }

        public string Content { get; }
    }
}