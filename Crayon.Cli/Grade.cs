using System;

namespace Crayon.Cli
{
    public class Grade
    {
        public Grade(
            string subject,
            string content,
            string teacherName,
            DateTimeOffset date,
            string type,
            string value)
        {
            Subject = subject;
            Content = content;
            TeacherName = teacherName;
            Date = date;
            Type = type;
            Value = value;
        }

        public string Subject { get; set; }

        public string Content { get; set; }

        public string TeacherName { get; set; }

        public string Type { get; set; }

        public DateTimeOffset Date { get; set; }

        public string Value { get; set; }
    }
}